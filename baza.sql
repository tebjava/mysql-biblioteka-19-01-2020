-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Sty 2020, 12:19
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `biblioteka`
--
CREATE DATABASE IF NOT EXISTS `biblioteka` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `biblioteka`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autorzy`
--

DROP TABLE IF EXISTS `autorzy`;
CREATE TABLE `autorzy` (
  `id_autor` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED DEFAULT NULL,
  `data_urodzin` date NOT NULL,
  `data_smierci` date DEFAULT '1970-01-01',
  `biografia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `autorzy`
--

INSERT INTO `autorzy` (`id_autor`, `id_osoba`, `data_urodzin`, `data_smierci`, `biografia`) VALUES
(1, 1, '1987-12-14', '1970-01-01', ''),
(2, 2, '1678-06-11', '1970-01-01', ''),
(3, 3, '1970-04-29', '1970-01-01', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `czytelnicy`
--

DROP TABLE IF EXISTS `czytelnicy`;
CREATE TABLE `czytelnicy` (
  `id_czytelnik` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED DEFAULT NULL,
  `numer_posesja` varchar(10) NOT NULL,
  `numer_lokal` varchar(10) NOT NULL,
  `wypozyczone` varchar(1000) NOT NULL,
  `id_magazyn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `czytelnicy`
--

INSERT INTO `czytelnicy` (`id_czytelnik`, `id_osoba`, `numer_posesja`, `numer_lokal`, `wypozyczone`, `id_magazyn`) VALUES
(1, 4, '67/40', '', '', NULL),
(2, 5, '15a', '67', '', NULL),
(3, 6, '56', '12', '', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ksiazki`
--

DROP TABLE IF EXISTS `ksiazki`;
CREATE TABLE `ksiazki` (
  `id_ksiazka` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(100) NOT NULL,
  `autorzy` varchar(400) DEFAULT NULL,
  `rok_wydania` year(4) DEFAULT NULL,
  `liczba_stron` mediumint(5) UNSIGNED DEFAULT NULL,
  `isbn` char(16) DEFAULT NULL,
  `opis` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyny`
--

DROP TABLE IF EXISTS `magazyny`;
CREATE TABLE `magazyny` (
  `id_magazyn` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL,
  `adres` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(30) NOT NULL,
  `nazwisko` varchar(50) NOT NULL,
  `kod_pocztowy` char(6) DEFAULT NULL,
  `miejscowosc` varchar(60) DEFAULT NULL,
  `ulica` varchar(100) DEFAULT NULL,
  `numer_posesja` varchar(10) DEFAULT NULL,
  `numer_lokal` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `kod_pocztowy`, `miejscowosc`, `ulica`, `numer_posesja`, `numer_lokal`) VALUES
(1, 'Jacek', 'Kawałkiewicz', NULL, NULL, NULL, NULL, NULL),
(2, 'Halina', 'Drozdowska', NULL, NULL, NULL, NULL, NULL),
(3, 'Marianna', 'Wężowska', NULL, NULL, NULL, NULL, NULL),
(4, 'Janina', 'Kowalska', '42-200', 'Częstochowa', 'Kwiatowa', '67/40', ''),
(5, 'Mariusz', 'Drozdowski', '00-150', 'Warszawa', 'Bohaterska', '15a', '67'),
(6, 'Alina', 'Jończyk', '00-150', 'Warszawa', 'Piłsudskiego', '56', '12');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

DROP TABLE IF EXISTS `pracownicy`;
CREATE TABLE `pracownicy` (
  `id_pracownik` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `stanowisko` varchar(30) NOT NULL,
  `pensja` decimal(7,2) NOT NULL,
  `id_magazyn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `autorzy`
--
ALTER TABLE `autorzy`
  ADD PRIMARY KEY (`id_autor`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `czytelnicy`
--
ALTER TABLE `czytelnicy`
  ADD UNIQUE KEY `id_czytelnik` (`id_czytelnik`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `ksiazki`
--
ALTER TABLE `ksiazki`
  ADD UNIQUE KEY `id_ksiazka` (`id_ksiazka`);

--
-- Indeksy dla tabeli `magazyny`
--
ALTER TABLE `magazyny`
  ADD UNIQUE KEY `id_magazyn` (`id_magazyn`);

--
-- Indeksy dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`);

--
-- Indeksy dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD UNIQUE KEY `id_pracownik` (`id_pracownik`),
  ADD KEY `id_osoba` (`id_osoba`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `autorzy`
--
ALTER TABLE `autorzy`
  MODIFY `id_autor` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `czytelnicy`
--
ALTER TABLE `czytelnicy`
  MODIFY `id_czytelnik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `ksiazki`
--
ALTER TABLE `ksiazki`
  MODIFY `id_ksiazka` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `magazyny`
--
ALTER TABLE `magazyny`
  MODIFY `id_magazyn` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  MODIFY `id_pracownik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `autorzy`
--
ALTER TABLE `autorzy`
  ADD CONSTRAINT `autorzy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `czytelnicy`
--
ALTER TABLE `czytelnicy`
  ADD CONSTRAINT `czytelnicy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD CONSTRAINT `pracownicy_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
